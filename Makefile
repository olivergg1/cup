# 'help' target by default
.PHONY: default test
default: help

## Remove build/ and vendor/ dirs and destroy containers
clean:
	rm -rf ./build
	rm -rf ./vendor

## Download dependencies into vendor/
deps:
	dep ensure -v -vendor-only

## Run tests
test:
	go test -v ./...

## Run tests with coverage tools and put result into build/
test-coverage:
	rm -rf ./build
	mkdir -p ./build
	go test -v ./... | tee ./build/report.txt
	go-junit-report < ./build/report.txt > ./build/report.xml
	gocoverutil -coverprofile=./build/coverage.out test -v ./...
	gocov convert ./build/coverage.out > ./build/coverage.json
	gocov-xml < ./build/coverage.json > ./build/coverage.xml
	cobertura-clover-transform ./build/coverage.xml > ./build/clover.xml
	gocov-html < ./build/coverage.json > ./build/coverage.html

## This help screen
help:
	$(info Available targets)
	@awk '/^[a-zA-Z\-\_0-9\.]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":")); \
			helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			printf "\033[1;32m %-20s \033[0m %s\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST)
